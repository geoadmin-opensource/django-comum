# coding: utf-8
from django.utils.translation import ugettext as _


TIPO_LINHA_COMERCIAL = "C"
TIPO_LINHA_RESIDENCIAL = "R"
TIPO_LINHA_AUTOMATICA = "A"

TIPO_LINHA_CHOICES = ((TIPO_LINHA_COMERCIAL, _(u"Comercial")),
                      (TIPO_LINHA_RESIDENCIAL, _(u"Residencial")),
                      (TIPO_LINHA_AUTOMATICA, _(u"Automática")))

SEXO_M = "M"
SEXO_F = "F"
SEXO_N = "N"

SEXO_CHOICES = ((SEXO_M, _(u"Masculino")),
                (SEXO_F, _(u"Feminino")),
                (SEXO_N, _(u"Não declarado")))

TIPO_LOGRADOURO = ["Aeroporto",
                   "Alameda",
                   "Área",
                   "Avenida",
                   "Campo",
                   "Chácara",
                   "Colônia",
                   "Condomínio",
                   "Conjunto",
                   "Distrito",
                   "Esplanada",
                   "Estação",
                   "Estrada",
                   "Favela",
                   "Fazenda",
                   "Feira",
                   "Jardim",
                   "Ladeira",
                   "Lago",
                   "Lagoa",
                   "Largo",
                   "Loteamento",
                   "Morro",
                   "Núcleo",
                   "Parque",
                   "Passarela",
                   "Pátio",
                   "Praça",
                   "Quadra",
                   "Recanto",
                   "Residencial",
                   "Rodovia",
                   "Rua",
                   "Setor",
                   "Sítio",
                   "Travessa",
                   "Trecho",
                   "Trevo",
                   "Vale",
                   "Vereda",
                   "Via",
                   "Viaduto",
                   "Viela",
                   "Vila"]

OPERADORA_TELEFONIA = ({"nome": "TIM", "codigo": "41"},
                       {"nome": "Oi", "codigo": "31"},
                       {"nome": "Vivo", "codigo": "15"},
                       {"nome": "Claro", "codigo": "20"},
                       {"nome": "Algar", "codigo": "12"})

TIPO_EMAIL = ["Pessoal", "Comercial", "Alternativo"]

