# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('municipios', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Anexo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data_hora_criacao', models.DateTimeField(help_text='Data/Hora da cria\xe7ao do objeto.', verbose_name='Data/Hora Cria\xe7\xe3o', auto_now_add=True)),
                ('data_hora_atualizacao', models.DateTimeField(help_text='Data/Hora da \xfaltima atualiza\xe7\xe3o do objeto.', verbose_name='Data/Hora', auto_now=True)),
                ('object_id', models.PositiveIntegerField(verbose_name='Dono Anexo', null=True, editable=False, blank=True)),
                ('nome', models.CharField(help_text='Nome do anexo.', max_length=128, verbose_name='Nome')),
                ('descricao', models.CharField(help_text='Descri\xe7\xe3o do anexo.', max_length=128, null=True, verbose_name='Descri\xe7\xe3o', blank=True)),
                ('arquivo', models.FileField(help_text='Arquivo', upload_to=b'anexos/', verbose_name='Arquivo')),
                ('content_type', models.ForeignKey(blank=True, editable=False, to='contenttypes.ContentType', null=True, verbose_name='ContentType')),
            ],
            options={
                'verbose_name': 'Anexo',
                'verbose_name_plural': 'Anexos',
            },
        ),
        migrations.CreateModel(
            name='Email',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('object_id', models.PositiveIntegerField(null=True, verbose_name='Id do Objeto Relacionado', blank=True)),
                ('email', models.EmailField(max_length=200)),
                ('content_type', models.ForeignKey(blank=True, editable=False, to='contenttypes.ContentType', null=True, verbose_name='Content-Type')),
            ],
            options={
                'ordering': ['tipoemail', 'email'],
                'verbose_name': 'Email',
            },
        ),
        migrations.CreateModel(
            name='Endereco',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('object_id', models.PositiveIntegerField(null=True, verbose_name='Id do Objeto Relacionado', blank=True)),
                ('nome_logradouro', models.CharField(max_length=128, verbose_name='Nome Logradouro')),
                ('numero', models.CharField(max_length=16, verbose_name='N\xfamero')),
                ('complemento', models.CharField(max_length=128, null=True, verbose_name='Complemento', blank=True)),
                ('cep', models.CharField(max_length=8, null=True, verbose_name='CEP', blank=True)),
                ('content_type', models.ForeignKey(blank=True, editable=False, to='contenttypes.ContentType', null=True, verbose_name='Content-Type')),
                ('municipio', models.ForeignKey(verbose_name='Munic\xedpio', to='municipios.Municipio')),
            ],
        ),
        migrations.CreateModel(
            name='Fotografia',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data_hora_criacao', models.DateTimeField(help_text='Data/Hora da cria\xe7ao do objeto.', verbose_name='Data/Hora Cria\xe7\xe3o', auto_now_add=True)),
                ('data_hora_atualizacao', models.DateTimeField(help_text='Data/Hora da \xfaltima atualiza\xe7\xe3o do objeto.', verbose_name='Data/Hora', auto_now=True)),
                ('object_id', models.PositiveIntegerField(verbose_name='Dono Fotografia', null=True, editable=False, blank=True)),
                ('nome', models.CharField(help_text='Nome da fotografia.', max_length=128, verbose_name='Nome')),
                ('descricao', models.CharField(help_text='Descri\xe7\xe3o da Fotografia.', max_length=128, null=True, verbose_name='Descri\xe7\xe3o', blank=True)),
                ('arquivo', models.ImageField(help_text='Fotografia', upload_to=b'fotografias/', verbose_name='Arquivo')),
                ('content_type', models.ForeignKey(blank=True, editable=False, to='contenttypes.ContentType', null=True, verbose_name='ContentType')),
            ],
            options={
                'verbose_name': 'Fotografia',
                'verbose_name_plural': 'Fotografias',
            },
        ),
        migrations.CreateModel(
            name='ModelListener',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('app', models.CharField(help_text='Aplica\xe7\xe3o Django que cont\xe9m o modelo em quest\xe3o', max_length=128, verbose_name='Aplica\xe7\xe3o Django')),
                ('modelo', models.CharField(help_text='Modelo Django que ser\xe1 utilizado.', max_length=128, verbose_name='Modelo Django')),
                ('name', models.CharField(max_length=64, verbose_name='Nome do monitorador')),
                ('active', models.BooleanField(default=True, help_text='Este monitorador est\xe1 ativo?', verbose_name='Ativo?')),
                ('post_save_handler', models.CharField(help_text='M\xe9todo que vai ser disparado para o evento que salva o post. Caminho completo do m\xe9todo, ex: meu_projeto.meu_aplicativo.handlers.handle_post_save', max_length=128, verbose_name='Monitorador de salvamento')),
                ('post_delete_handler', models.CharField(help_text='M\xe9todo que vai ser disparado para o evento que apaga o post. Caminho completo do m\xe9todo, ex: meu_projeto.meu_aplicativo.handlers.handle_post_delete', max_length=128, verbose_name='Monitorador de exclus\xe3o')),
                ('listen_post_save', models.BooleanField(default=True, help_text='Determina se o modelo vai monitorar eventos de salvamento.', verbose_name='Monitora salvamento?')),
                ('listen_post_delete', models.BooleanField(default=True, help_text='Determina se o modelo vai monitorar eventos de exclus\xe3o.', verbose_name='Monitora exclus\xe3o?')),
                ('auto_on', models.BooleanField(default=False, verbose_name='In\xedcio autom\xe1tico?')),
            ],
            options={
                'verbose_name': 'Monitorador de Modelo',
                'verbose_name_plural': 'Monitoradores de Modelo',
            },
        ),
        migrations.CreateModel(
            name='OperadoraTelefonia',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(help_text='Nome da operadora', unique=True, max_length=32, verbose_name='Nome')),
                ('codigo', models.PositiveIntegerField(help_text='C\xf3digo da operadora de telefonia, exemplo: EMBRATEL: 21', unique=True, verbose_name='C\xf3digo')),
            ],
        ),
        migrations.CreateModel(
            name='Telefone',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ddd', models.PositiveIntegerField(default=34, help_text='C\xf3digo de discagem direta.', verbose_name='DDD')),
                ('telefone', models.CharField(help_text='N\xfamero do telefone', max_length=15, verbose_name='Telefone')),
                ('ramal', models.PositiveIntegerField(null=True, blank=True)),
                ('tipo_linha', models.CharField(default=b'C', help_text="Tipo da linha, exemplo: 'residencial','comercial', etc.", max_length=1, verbose_name='Tipo de Linha', choices=[(b'C', 'Comercial'), (b'R', 'Residencial'), (b'A', 'Autom\xe1tica')])),
                ('object_id', models.PositiveIntegerField(null=True, verbose_name='Id do Objeto Relacionado', blank=True)),
                ('content_type', models.ForeignKey(blank=True, editable=False, to='contenttypes.ContentType', null=True, verbose_name='Content-Type')),
                ('operadora', models.ForeignKey(blank=True, to='comum.OperadoraTelefonia', help_text='Operadora que suporta este n\xfamero.', null=True, verbose_name='Operadora')),
            ],
        ),
        migrations.CreateModel(
            name='TipoEmail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('valor', models.CharField(help_text="Valor utilizado para substituir o c\xf3digo no sistema. Exemplo: 'Asfalto' para 'AS'.", unique=True, max_length=128, verbose_name='Valor')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='TipoLogradouro',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data_hora_criacao', models.DateTimeField(help_text='Data/Hora da cria\xe7ao do objeto.', verbose_name='Data/Hora Cria\xe7\xe3o', auto_now_add=True)),
                ('data_hora_atualizacao', models.DateTimeField(help_text='Data/Hora da \xfaltima atualiza\xe7\xe3o do objeto.', verbose_name='Data/Hora', auto_now=True)),
                ('valor', models.CharField(help_text="Valor utilizado para substituir o c\xf3digo no sistema. Exemplo: 'Asfalto' para 'AS'.", unique=True, max_length=128, verbose_name='Valor')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='endereco',
            name='tipo_logradouro',
            field=models.ForeignKey(related_name='enderecos', verbose_name='Tipo Logradouro', to='comum.TipoLogradouro'),
        ),
        migrations.AddField(
            model_name='email',
            name='tipoemail',
            field=models.ForeignKey(related_name='emails', verbose_name='Tipo Email', to='comum.TipoEmail'),
        ),
    ]
