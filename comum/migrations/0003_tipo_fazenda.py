from __future__ import unicode_literals
from django.db import migrations
from ..models import TipoLogradouro


def tipo_fazenda(apps, schema_editor):
    if TipoLogradouro.objects.filter(valor='Fazenda').exists():
        pass

    else:
        TipoLogradouro.objects.create(valor='Fazenda')


class Migration(migrations.Migration):

    dependencies = [
        ('comum', '0002_dados_padrao'),
    ]

    operations = [
        migrations.RunPython(tipo_fazenda),
    ]
