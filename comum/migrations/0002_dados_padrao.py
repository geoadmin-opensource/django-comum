# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import migrations
from .. import choices
from ..models import TipoLogradouro, OperadoraTelefonia, TipoEmail


def cria_dados(apps, schema_editor):
    for tipo in choices.TIPO_LOGRADOURO:
        TipoLogradouro.objects.create(valor=tipo)

    for tipoe in choices.TIPO_EMAIL:
        TipoEmail.objects.create(valor=tipoe)

    for operadora in choices.OPERADORA_TELEFONIA:
        OperadoraTelefonia.objects.create(nome=operadora['nome'],
                                          codigo=operadora['codigo'])


class Migration(migrations.Migration):

    dependencies = [
        ('comum', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(cria_dados),
    ]
