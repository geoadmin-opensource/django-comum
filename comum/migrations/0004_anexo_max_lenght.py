# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('comum', '0003_tipo_fazenda'),
    ]

    operations = [
        migrations.AlterField(
            model_name='anexo',
            name='nome',
            field=models.CharField(help_text='Nome do anexo.', max_length=300, verbose_name='Nome'),
        ),
    ]
